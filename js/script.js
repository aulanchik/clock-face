const HOURSARM = document.querySelector("#hour");
const MINUTESARM = document.querySelector("#minute");
const SECONDSARM = document.querySelector("#second");
const TIMESTEP = 1000;

/**
 * Parse current time
 */
var date = new Date();
var hours = date.getHours();
var minutes = date.getMinutes();
var seconds = date.getMinutes();

/**
 * Initial degree position for each arm
 */
let hourPosition = (hours * 360 / 12) + (minutes * (360 / 60) / 12);
let minutePosition = (minutes * 360 / 60) + (seconds * (360 / 60) / 12);
let secondPosition = seconds * 360 / 60;

function animateClock() {

    hourPosition += (3 / 360);
    minutePosition += (6 / 60);
    secondPosition += 6;

    HOURSARM.style.transform = "rotate(" + hourPosition + "deg)";
    MINUTESARM.style.transform = "rotate(" + minutePosition + "deg)";
    SECONDSARM.style.transform = "rotate(" + secondPosition + "deg)";
}

var interval = setInterval(animateClock, TIMESTEP);